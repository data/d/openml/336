# OpenML dataset: SPECT

https://www.openml.org/d/336

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Krzysztof J. Cios","Lukasz A.  
**Source**: [original](https://archive.ics.uci.edu/ml/datasets/SPECT+Heart) -   
**Please cite**:   

SPECT heart data

This is a merged version of the separate train and test set which are usually distributed. On OpenML this train-test split can be found as one of the possible tasks.  

Sources: 
-- Original owners: Krzysztof J. Cios, Lukasz A. Kurgan University of Colorado at Denver, Denver, CO 80217, U.S.A. Krys.Cios@cudenver.edu Lucy S. Goodenday Medical College of Ohio, OH, U.S.A.  
-- Donors: Lukasz A.Kurgan, Krzysztof J. Cios 
-- Date: 10/01/01  

Relevant Information: The dataset describes diagnosing of cardiac Single Proton Emission Computed Tomography (SPECT) images.  Each of the patients is classified into two categories: normal and abnormal.  The database of 267 SPECT image sets (patients) was processed to extract features that summarize the original SPECT images.  As a result, 44 continuous feature pattern was created for each patient. The pattern was further processed to obtain 22 binary feature patterns. The CLIP3 algorithm was used to generate classification rules from these patterns. The CLIP3 algorithm generated rules that were 84.0% accurate (as compared with cardiologists' diagnoses).   

Attribute Information: 
1. OVERALL_DIAGNOSIS: 0,1 (class attribute, binary) 
2. F1: 0,1 (the partial diagnosis 1, binary) 
3. F2: 0,1 (the partial diagnosis 2, binary) 
4. F3: 0,1 (the partial diagnosis 3, binary) 
5. F4: 0,1 (the partial diagnosis 4, binary) 
6. F5: 0,1 (the partial diagnosis 5, binary) 
7. F6: 0,1 (the partial diagnosis 6, binary) 
8. F7: 0,1 (the partial diagnosis 7, binary) 
9. F8: 0,1 (the partial diagnosis 8, binary) 
10. F9: 0,1 (the partial diagnosis 9, binary) 
11. F10: 0,1 (the partial diagnosis 10, binary) 
12. F11: 0,1 (the partial diagnosis 11, binary) 
13. F12: 0,1 (the partial diagnosis 12, binary) 
14. F13: 0,1 (the partial diagnosis 13, binary) 
15. F14: 0,1 (the partial diagnosis 14, binary) 
16. F15: 0,1 (the partial diagnosis 15, binary) 
17. F16: 0,1 (the partial diagnosis 16, binary) 
18. F17: 0,1 (the partial diagnosis 17, binary) 
19. F18: 0,1 (the partial diagnosis 18, binary) 
20. F19: 0,1 (the partial diagnosis 19, binary) 
21. F20: 0,1 (the partial diagnosis 20, binary) 
22. F21: 0,1 (the partial diagnosis 21, binary) 
23. F22: 0,1 (the partial diagnosis 22, binary)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/336) of an [OpenML dataset](https://www.openml.org/d/336). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/336/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/336/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/336/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

